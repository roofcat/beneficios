var zonas, regiones, ciudades, comunas, segmentos, categorias, subcategorias, convenios;
var retorno;

$( document ).on( "ready", function () {
	$.ajax({
		type: "get",
		url: "https://script.google.com/macros/s/AKfycbzr0bSt3_cXBJjSK6QB5WyWkoDd8EzrOJaAcZ-3C9wDrPguTCI/exec",
		success: function ( data ) {
			zonas = data.zonas;
			regiones = data.regiones;
			ciudades = data.ciudades;
			comunas = data.comunas;
			segmentos = data.segmentos;
			categorias = data.categorias;
			subcategorias = data.subCategorias;
			convenios = data.convenios;

			llenarComboZona( zonas );
			llenarComboSegmento( segmentos );
			llenarComboCategoria( categorias );
			llenarComboSubcategoria( subcategorias );
			llenarComboConvenio( convenios );
		},
		error: function ( jqXHR, textStatus, errorThrown ) {
			alert( errorThrown );
		}
	});
});
function llenarComboZona ( zonas ) {
	var comboZona = $("#ComboZona");
	limpiarComboZona();
	limpiarComboRegion();
	limpiarComboCiudad();
	limpiarComboComuna();
	for ( var i = 0; i < zonas.length; i++ ) {
		comboZona.append('<option value="' + zonas[i].idZona + '">' + zonas[i].zona + '</option>');
	};
};
function llenarComboRegion () {
	var comboZona = $("#ComboZona").val();
	var comboRegion = $ ("#ComboRegion");
	limpiarComboRegion();
	limpiarComboCiudad();
	limpiarComboComuna();
	for ( var i = 0; i < regiones.length; i++ ) {
		if ( regiones[i].idZona == comboZona ) {
			comboRegion.append('<option value="' + regiones[i].idRegion + '">' + regiones[i].region + '</option>');
		};
	};
};
function llenarComboCiudad () {
	var comboRegion = $("#ComboRegion").val();
	var comboCiudad = $("#ComboCiudad");
	limpiarComboCiudad();
	limpiarComboComuna();
	for ( var i = 0; i < ciudades.length; i++ ) {
		if ( ciudades[i].idRegion == comboRegion ) {
			comboCiudad.append('<option value="' + ciudades[i].idCiudad + '">' + ciudades[i].ciudad + '</option>');
		};
	};
};
function llenarComboComuna () {
	var comboCiudad = $("#ComboCiudad").val();
	var comboComuna =$("#ComboComuna");
	limpiarComboComuna();
	for ( var i = 0; i < comunas.length; i++ ) {
		if ( comunas[i].idCiudad == comboCiudad ) {
			comboComuna.append('<option value="' + comunas[i].idComuna + '">' + comunas[i].comuna + '</option>');
		};
	};
};
function llenarComboConvenio ( convenios ) {
	var ComboConvenio = $("#ComboConvenio");
	limpiarComboConvenio();
	for ( var i = 0; i < convenios.length; i++ ) {
		ComboConvenio.append('<option value="' + i + '">' + convenios[i] + '</option>');
	};
};
function llenarComboSubcategoria ( subcategorias ) {
	var ComboSubCategoria = $("#ComboSubCategoria");
	limpiarComboSubCategoria();
	for ( var i = 0; i < subcategorias.length; i++ ) {
		ComboSubCategoria.append('<option value="' + i + '">' + subcategorias[i] + '</option>');
	};
};
function llenarComboCategoria ( categorias ) {
	var ComboCategoria = $("#ComboCategoria");
	limpiarComboCategoria();
	for ( var i = 0; i < categorias.length; i++ ) {
		ComboCategoria.append('<option value="' + i + '">' + categorias[i] + '</option>');
	};	
};
function llenarComboSegmento ( segmentos ) {
	var ComboSegmento = $("#ComboSegmento");
	limpiarComboSegmento();		
	for ( var i = 0; i < segmentos.length; i++ ) {
		ComboSegmento.append('<option value="' + i + '">' + segmentos[i] + '</option>');
	};
};
function limpiarComboConvenio () {
	var combo = $("#ComboConvenio");
	combo.empty();
	combo.append('<option value="">Convenio...</option>');
};
function limpiarComboSubCategoria () {
	var combo = $("#ComboSubCategoria");
	combo.empty();
	combo.append('<option value="">Subcategoría...</option>');
};
function limpiarComboCategoria () {
	var combo = $("#ComboCategoria");
	combo.empty();
	combo.append('<option value="">Categoría...</option>');
};
function limpiarComboSegmento () {
	var combo = $("#ComboSegmento");
	combo.empty();
	combo.append('<option value="">Segmento...</option>');
};
function limpiarComboZona () {
	var combo = $( "#ComboZona" );
	combo.empty();
	combo.append('<option value="">Zona...</option>');
};
function limpiarComboRegion () {
	var combo = $( "#ComboRegion" );
	combo.empty();
	combo.append('<option value="">Región...</option>');
};
function limpiarComboCiudad () {
	var combo = $( "#ComboCiudad" );
	combo.empty();
	combo.append('<option value="">Ciudad...</option>');
};
function limpiarComboComuna () {
	var combo = $( "#ComboComuna" );
	combo.empty();
	combo.append('<option value="">Comuna...</option>');
};
function buscar () {
	var textoComercio = $("#institucionTxt").val();

	var comboZona = $("#ComboZona");
	if ( comboZona.val() == 0 ) {
		var textoZona = "";
	} else {
		var textoZona = comboZona.find("option:selected").text();
	}
	var comboRegion = $("#ComboRegion");
	if ( comboRegion.val() == 0) {
		var textoRegion = "";
	} else {
		var textoRegion = comboRegion.find("option:selected").text();
	}
	var comboCiudad = $("#ComboCiudad");
	if ( comboCiudad.val() == 0 ) {
		var textoCiudad = "";
	} else {
		var textoCiudad = comboCiudad.find("option:selected").text();
	}
	var comboComuna = $("#ComboComuna");
	if ( comboComuna.val() == 0 ) {
		var textoComuna = "";
	} else {
		var textoComuna = comboComuna.find("option:selected").text();
	}
	var comboSegmento = $("#ComboSegmento");
	if ( comboSegmento.val() == 0 ) {
		var textoSegmento = "";
	} else {
		var textoSegmento = comboSegmento.find("option:selected").text();
	}
	var comboCategoria = $("#ComboCategoria");
	if ( comboCategoria.val() == 0 ) {
		var textoCategoria = "";
	} else {
		var textoCategoria = comboCategoria.find("option:selected").text();
	}
	var comboSubCategoria = $("#ComboSubCategoria");
	if (comboSubCategoria.val() == 0) {
		var textoSubCategoria = "";
	} else {
		var textoSubCategoria = comboSubCategoria.find("option:selected").text();
	}
	var comboConvenio = $("#ComboConvenio");
	if ( comboConvenio.val() == 0 ) {
		var textoConvenio = "";
	} else {
		var textoConvenio = comboConvenio.find("option:selected").text();
	}

	obtenerDatos(textoComercio, textoZona, textoRegion, textoCiudad, textoComuna, textoSegmento, textoCategoria, textoSubCategoria, textoConvenio);
};
function obtenerDatos ( comercio, zona, region, ciudad, comuna, segmento, categoria, subcategoria, convenio ) {
	$.ajax({
		type: "get",
		url: "https://script.google.com/macros/s/AKfycbxyoF-54zfV4S2QI1yXTc1UsfCOLJUh3BrJOM59el4u-tec6-A/exec",
		data: {
			comercio: comercio,
			zona: zona,
			region: region,
			ciudad: ciudad,
			comuna: comuna,
			segmento: segmento,
			categoria: categoria,
			subcategoria: subcategoria,
			convenio: convenio,
		},
		success: function ( data ) {
			llenarTabla(data);
		},
		error:  function ( errorThrown ) {
			console.log(errorThrown);
		}
	});
};
function llenarTabla ( data ) {
	var meses = new Array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
	var dias = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
	var laTabla = $("#tablaBody");
	laTabla.empty();
	for ( var i = 0; i < data.length; i++ ) {
		laTabla.append('<tr>');
		laTabla.append('<td>' + data[i].comercio + '</td>');
		laTabla.append('<td>' + data[i].zona + '</td>');
		laTabla.append('<td>' + data[i].region + '</td>');
		laTabla.append('<td>' + data[i].ciudad + '</td>');
		laTabla.append('<td>' + data[i].comuna + '</td>');
		laTabla.append('<td>' + data[i].segmento + '</td>');
		laTabla.append('<td>' + data[i].categoria + '</td>');
		laTabla.append('<td>' + data[i].subcategoria + '</td>');
		laTabla.append('<td>' + data[i].convenio + '</td>');
		laTabla.append('<td>' + data[i].ahorroEstimado + '</td>');
		laTabla.append('<td>' + data[i].formaDeAccederAlBeneficio + '</td>');
		laTabla.append('<td>' + data[i].direccion + '</td>');
		laTabla.append('<td>' + moment(data[i].vigencia).format("D/MM/YYYY") + '</td>');
		laTabla.append('<td>' + data[i].legales + '</td>');
		laTabla.append('<td><a class="btn btn-xs" href="' + data[i].linkAMasInformacion +'" target="_blank">Más info</a></td>');
		laTabla.append('</tr>');
	}
};