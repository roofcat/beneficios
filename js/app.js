var app = angular.module("myApp", [
	"ngRoute", 
	"ngResource",
	"ngGrid"
	]);

app.controller('BeneficiosController', function ($scope, $http) {
	delete $http.defaults.headers.common["X-Requested-With"];

	$scope.datos = {};
	var url = "https://script.google.com/a/macros/azurian.com/s/AKfycbz6_qG8M8BGqJO1gCTN4EpabR9tvT_3VYJ-fyczfWIkjAPrUA8/exec";
	//var url = "https://script.google.com/macros/s/AKfycbwU8Z3BA2voDRiW-Bmg_MR7ZCYGioKFHaByUTm0vfjUZQyTJfRU/exec";

	$http.get(url).success(function(data){
		$scope.datos = data;
	}).error(function(data) {
		console.log("Mensaje : " + data);
	});

	$scope.gridOptions = {
		data: 'datos'
	};
});