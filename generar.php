<?php

require_once 'fpdf/fpdf.php';

class PDF extends FPDF
{
	function Header ()
	{
		$this->Image('imagenes/araucana.gif', 10, 8, 33);
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(80);
		$this->Cell(30, 10, 'La Araucana Beneficios', 0, 0, 'C');
		$this->Ln(20);
	}

	function Footer ()
	{
		$this->SetY(-15);
		$this->SetFont('Arial', 'I', 8);
		$this->Cell(0, 10, iconv("UTF-8", "ISO-8859-1", 'Página ' . $this->PageNo() . '/{nb}'), 0 , 0, 'C');
	}
}

$pdf = new PDF('L', 'mm', 'A4');
$pdf -> Addpage();
$pdf -> SetFont('Arial', 'B', 16);
$pdf -> Cell (40, 10, iconv("UTF-8", "ISO-8859-1", "¡Hola, Mundo!"), 0);
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Cell (40, 10, iconv('UTF-8', 'ISO-8859-1', "Santiago, "), 0);
$pdf -> Output();